require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})

module.exports = {
  siteMetadata: {
    title: `Taco Love | Berlin Tacos From The Heart`,
    description: `Homemade corn tortillas wrapped around a healthy, hearty, fusion of international ingredients, delivered right to your hands. Made with love in Berlin. `,
    author: `@tacolove`,
  },
  plugins: [
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: "UA-167196976-1",
        // this option places the tracking script into the head of the DOM
        head: true,
        // other options
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Taco Love`,
        short_name: `Taco Love`,
        start_url: `/shop`,
        background_color: `#663399`,
        display: "standalone",
        theme_color: `#663399`,
        icon: `src/images/logos/logo-mark.png`, // This path is relative to the root of the site.
      },
    },
    'gatsby-plugin-resolve-src',
    `gatsby-plugin-fastclick`,
    `gatsby-plugin-styled-components`,
    {
      resolve: 'gatsby-source-prismic-graphql',
      options: {
        repositoryName: 'taco-love', // (REQUIRED, replace with your own)
        accessToken: `${process.env.GATSBY_PRISMIC_API_KEY}`, // (optional API access token)
        path: '/preview', // (optional preview path. Default: /preview)
        previews: true, // (optional, activated Previews. Default: false)
      }
    },
  {
    resolve: `gatsby-plugin-google-fonts`,
    options: {
      fonts: [
        `quicksand`,
        `josefin slab`
      ],
      display: 'swap'
    }
  },
    `gatsby-plugin-react-helmet`,
  ],
}
