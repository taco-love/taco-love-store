require("dotenv").config({
    path: `.env.development`,
})

console.log(process.env, 'env')
const axios = require('axios')

exports.handler = async function(event, context, callback) {
    if (event.httpMethod === 'OPTIONS') {
        preflight(callback);
        return
    }

    const order = JSON.parse(event.body)


    const { TRELLO_KEY, TRELLO_TOKEN, TRELLO_ORDER_LIST_ID} = process.env

    const { deliveryLocation: {latitude, longitude}} = order
    const request = {
        name: getCardTitle(order),
        desc: getCardDescription(order),
    }
    if(order.deliveryType.includes('delivery')) {
        request.coordinates =  `${latitude},${longitude}`
        request.address = order.deliveryAddress
    }
    const result = await axios.post(`https://api.trello.com/1/cards?idList=${TRELLO_ORDER_LIST_ID}&key=${TRELLO_KEY}&token=${TRELLO_TOKEN}`,
        request)

    callback(null, {
        statusCode: 200,
        body:'Success',
        headers:{
            'Access-Control-Allow-Origin': '*',
        },
    });
}


const getCardTitle = (order) => {
    let name  = `Order for ${order.customerName} - ${order.deliveryType.includes('deliver') ? 'delivery' : order.deliveryType} 
    - ${order.totalQuantity} total item(s) `
    order.items.map((item) => {
        name += `----- [${item.quantity}x ${item.name}]  `
    })

    return name
}

const getCardDescription = (order) => {

    let description = `**Order #${order.id}** - ${order.deliveryType.includes('deliver') ? 'delivery' : order.deliveryType}  \n`
    + order.items.map(item => `- ${item.quantity}x ${item.name}\n`).join('')
    + `\n \`\`\`Total: ${order.totalQuantity} item(s)\`\`\``
    + `\n \`\`\`Cost: €${order.totalPrice}\`\`\`\n`
    + `\n\nCustomer Name: ${order.customerName}\n`
    + `Customer Phone: ${order.customerPhone} \n\n`

    console.log(order.deliveryType, 'deliveryType')
    if(order.deliveryType.toLowerCase().includes('address')) {
        description += `Delivery Address: ${order.deliveryAddress}\n\n`
        + `https://www.google.com/maps/search/?api=1&query=${encodeURI(order.deliveryAddress)}`

        if(order.deliveryNotes) {
            description += `\n\n Notes: ${order.deliveryNotes}`
        }
    }

    if(order.deliveryType.toLowerCase().includes('location')) {
        description += `Delivery to Location`
    }

    return description
}


const isDelivery = (order) => {
    return order.deliveryType.includes('delivery')
}
function preflight(callback) {
    callback(null, {
        statusCode: 204,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'GET, POST, PUT',
            'Access-Control-Allow-Headers' :'*'
        },
        body: '',
    });
}
