import styled from "styled-components"
import { Flex } from "reflexbox"
const Container = styled.div`
  padding: ${({ padding }) => padding || "10px"};
  padding-top: ${({ menuPadding }) => menuPadding || "50px"};
  width: 100%;

  /* Firefox */
  height: -moz-calc(100% - 50px);
  /* WebKit */
  height: -webkit-calc(100% - 50px);
  /* Opera */
  height: -o-calc(100% - 50px);
  /* Standard */
  height: calc(100% - 50px);
`

export default Container
