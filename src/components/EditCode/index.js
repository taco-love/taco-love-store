import React from "react"
import { Icon } from "semantic-ui-react"
import vCardsJS from "vcards-js"
import { navigate, Link } from "gatsby"
import styled from "styled-components"
import { Flex, Box as ReflexBox } from "reflexbox"
import vCardParse from "vcf"
import Layout from "components/layout"
import Input from "components/Input"
import Button from "components/Button"
import create from "entities/codes/functions/create"
import update from "entities/codes/functions/update"
import get from "entities/codes/functions/get"
import setCurrentType from "entities/codes/functions/setCurrent"
const Description = styled.div`
  margin: 10px 0px 30px 0px;
`

class EditPage extends React.Component {
  state = {
    qrContents: "",
  }

  submitForm = async () => {
    const { qrContents } = this.state
    const card = await get(this.codeType)

    if (!card) await create(qrContents, this.codeType)
    else await update(qrContents, this.codeType)
    await setCurrentType(this.codeType)
    await fetch("/qrContents", {
      method: "POST",
      body: JSON.stringify({
        content: { qrContents: qrContents, type: this.codeType },
      }),
    })
    navigate("/")
  }

  render() {
    return null
  }
}

export default EditPage
