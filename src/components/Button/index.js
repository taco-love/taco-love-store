import styled from "styled-components"
import { teal } from "styles/colors"

const StyledButton = styled.button`
  background-color: ${teal};
  padding: 10px;
  height: 60px;
  ${({ fluid }) => (fluid ? "width: 100%" : "")};
  box-shadow: inset 0 0 0 0 rgba(34, 36, 38, 0.15);
  text-align: center;
  color: white;

  &:hover {
    cursor: pointer;
    background-color: #05928c;
    transition: 0.2s;
  }
`

export default StyledButton
