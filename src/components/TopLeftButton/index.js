import { Button } from "semantic-ui-react"
import PropTypes from "prop-types"
import React from "react"

const TopLeftButton = ({ onClick, children }) => (
  <Button
    color="green"
    style={{ position: "absolute", top: 5, left: 5, zIndex: 5000 }}
    onClick={onClick}
  >
    {children}
  </Button>
)

TopLeftButton.propTypes = {
  onClick: PropTypes.func,
  children: PropTypes.node,
}

export default TopLeftButton
