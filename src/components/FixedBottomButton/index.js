import React from "react"
import PropTypes from "prop-types"
import styled from "styled-components"
import { Button } from "semantic-ui-react"
import { Flex } from "reflexbox"
import FadeIn from "react-fade-in"
const Centered = styled(Flex)`
  align-items: center;
  justify-content: center;
  text-align: center;
  position: fixed;
  bottom: 10px;
  width: 80%;
  margin: 0 auto;
  z-index: 5000;
  max-width: 400px;

  & .fadeIn {
    width: 100%;
  }
`

const Container = styled(Flex)`
  align-items: center;
  justify-content: center;
  padding: 8px;
  text-align: center;
`

const StyledButton = styled(Button)`
  width: 100%;
  &&& {
    transition: 0.5s all;
  }
`
const FixedBottomButton = ({ children, ...restProps }) => {
  return (
    <Container>
      <Centered>
        <FadeIn className="fadeIn">
          <StyledButton size="large" {...restProps}>
            {children}
          </StyledButton>
        </FadeIn>
      </Centered>
    </Container>
  )
}

FixedBottomButton.propTypes = {
  children: PropTypes.node,
}

export default FixedBottomButton
