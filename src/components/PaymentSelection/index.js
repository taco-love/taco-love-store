import React from "react"
import Layout from "components/layout"
import SEO from "components/seo"
import { navigate } from "gatsby"
import styled from "styled-components"
import { Flex } from "reflexbox"
import { Icon, Button } from "semantic-ui-react"
import FixedBottomButton from "components/FixedBottomButton"
const Centered = styled(Flex)`
  align-items: center;
  justify-content: center;
  text-align: center;
`

const Container = styled(Flex)`
  align-items: center;
  justify-content: center;
  height: 100vh;
  padding: 8px;
`

const Selection = styled(Flex)`
  align-items: center;
  justify-content: center;
  text-align: center;
  min-height: 100px;
  border-radius: 5px;
  border: 1px solid rgba(0, 0, 0, 0.6);
  width: 32%;
  & > i {
  }
  ${({ selected }) =>
    selected &&
    "color:black;border: 3px solid #fbbd08;background-color: rgba(251,189,8,0.3);"}
  ${({ selected }) =>
    !selected &&
    "color:rgba(0,0,0,0.6);border: 1px solid rgba(0,0,0,0.6);background-color: white"}
    transition: 0.3s all;
`

const SELECTION_TYPES = {
  delivery_address: "delivery_address",
  delivery_location: "delivery_location",
  pickup: "pickup",
}
class IndexPage extends React.Component {
  state = {
    selected: SELECTION_TYPES.delivery_address,
  }

  switchSelected = type => {
    this.setState({ selected: type })
  }

  navigateToNext = () => {
    const { selected } = this.state
    if (selected === SELECTION_TYPES.delivery_address) {
      navigate("/delivery/address")
    }

    if (selected === SELECTION_TYPES.delivery_location) {
      navigate("/delivery/location")
    }

    if (selected === SELECTION_TYPES.pickup) {
      navigate("/")
    }
  }

  render() {
    const { selected } = this.state
    return (
      <Flex justifyContent="space-between" flexWrap="wrap">
        <Selection
          mb={3}
          flexWrap="wrap"
          justifyContent="centered"
          selected={selected === "delivery_address"}
          onClick={() => this.switchSelected("delivery_address")}
        >
          <div style={{ display: "inherit" }}>
            <div style={{ marginRight: "5px", width: "100%" }}>
              <Icon name="money" size="big" />
              <Centered>Cash</Centered>
            </div>
            <br />
          </div>
        </Selection>
        <Selection
          mb={3}
          selected={selected === SELECTION_TYPES.delivery_location}
          onClick={() => {}}
        >
          <div style={{ display: "inherit" }}>
            <div style={{ marginRight: "5px" }}>
              <Icon name="credit card outline" size="big" />
              <Centered>Credit (Coming Soon)</Centered>
            </div>
          </div>
        </Selection>
        <Selection
          mb={3}
          selected={selected === SELECTION_TYPES.delivery_location}
          onClick={() => {}}
        >
          <div style={{ display: "inherit" }}>
            <div style={{ marginRight: "5px" }}>
              <Icon name="paypal" size="big" />
              <Centered>Paypal (Coming Soon)</Centered>
            </div>
          </div>
        </Selection>
      </Flex>
    )
  }
}

export default IndexPage
