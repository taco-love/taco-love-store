import React from "react"
import PropTypes from "prop-types"
import styled from "styled-components"
import { Box } from "reflexbox"

const StyledBox = styled(Box)`
  background: ${({ bgColor }) => bgColor};
  background-color: ${({ bgColor }) => (bgColor ? bgColor : "inherit")};
  background-image: ${({ bgImage }) => (bgImage ? bgImage : "none")};
  color: ${({ color }) => (color ? color : "inherit")};
`

const Section = ({
  children,
  className,
  bgColor,
  bgImage,
  color,
  width,
  padding,
  ...restProps
}) => {
  let boxWidth = width
  if (!boxWidth) boxWidth = 1
  return (
    <StyledBox
      pd={padding}
      className={className}
      width={boxWidth}
      bgColor={bgColor}
      bgImage={bgImage}
      color={color}
      {...restProps}
    >
      {children}
    </StyledBox>
  )
}

Section.propTypes = {
  children: PropTypes.node.isRequired,
  bgColor: PropTypes.string,
  bgImage: PropTypes.string,
  color: PropTypes.string,
  width: PropTypes.node,
  className: PropTypes.string,
  padding: PropTypes.bool,
}

Section.defaultProps = {
  padding: false,
}

export default Section
