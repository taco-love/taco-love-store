/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"
import { Helmet } from "react-helmet"
import "./layout.css"
import styled, { createGlobalStyle } from "styled-components"
import reset from "styled-reset-advanced"
import "semantic-ui-css/semantic.min.css"
import Navbar from "components/Navbar"
import Container from "components/Container"
import { primary } from "styles/colors"
import Seo from "components/seo"
import ogImg from "images/ogImg.png"
import Gradient from "components/Gradient"
const GlobalStyle = createGlobalStyle`
  ${reset};
  
  html, body { height: 100% }
  
  html {
  height: initial;
}

  canvas {
   width:300px;
   height: 300px;
   margin:0 auto;
   border: 2px dotted black;
   }
   
   body {
   -webkit-overflow-scrolling: touch;
   overflow: scroll;
     font-size:18px;
     font-family: 'quicksand', sans-serif;
   
   }
   
   a {
        text-decoration: underline;
        
    }
    a:hover {
      color: ${primary.o100}
    }
   
   h1 {
   font-size: 20px;
    line-height: 1.28571429em;
    font-weight: 700;
    }
    
    h2 {
        font-size:18px
        margin-bottom:10px;
    }
    
    p {
        margin:10px 0px;
    }
    
    strong {
        font-weight: 600;
    }
    
   img {
   display:block;
   }
   
     *, *:before, *:after {
    user-select: auto;
  }
`

const LayoutContainer = styled.div`
  min-height: 100vh;
  position: relative;
`

const Layout = ({
  children,
  navLeftComponent,
  navRightComponent,
  navTitle,
  menuPadding,
  padding,
  gradient,
}) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
          description
        }
      }
    }
  `)

  return (
    <>
      <Seo
        title={data.site.siteMetadata.title}
        ogTitle="Taco Love: Berlin Tacos From The Heart"
        description="Homemade corn tortillas wrapped around a healthy, hearty, fusion of international ingredients, delivered right to your hands. Made with love in Berlin."
        url="https://tacoloveberlin.com"
        image={ogImg}
      />
      <GlobalStyle />
      <LayoutContainer>
        <Navbar
          LeftComponent={navLeftComponent}
          RightComponent={navRightComponent}
          title={navTitle}
        />
        {gradient ? (
          <Container padding={padding} menuPadding={menuPadding}>
            <Gradient>{children}</Gradient>
          </Container>
        ) : (
          <Container padding={padding} menuPadding={menuPadding}>
            {children}
          </Container>
        )}
      </LayoutContainer>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
  navLeftComponent: PropTypes.node,
  navRightComponent: PropTypes.node,
  navTitle: PropTypes.string,
  menuPadding: PropTypes.string,
  padding: PropTypes.string,
  gradient: PropTypes.bool,
}

export default Layout
