import styled from "styled-components"

const Margin = styled.div`
  margin: ${({ margin }) => margin};
`

export default Margin
