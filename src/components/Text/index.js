import styled from "styled-components"
import { white } from "styles/colors"
import media from "styles/media"

export const Text = styled.div`
  overflow-wrap: break-word;
  line-height: ${({ lHeight }) => (lHeight ? lHeight : "24px")};
  letter-spacing: ${({ spacing }) => (spacing ? `${spacing}px` : "0.5px")};
  font-weight: ${({ weight }) => (weight ? weight : "600")};
  font-size: ${({ size }) => (size ? size : "16px")};
  color: ${({ color }) => (color ? color : "inherit")};
  margin: ${({ margin }) => margin && margin};
  margin-left: ${({ marginLeft }) => marginLeft && marginLeft};
  text-align: ${({ align }) => align && align};
  text-transform: ${({ uppercase }) => uppercase && "uppercase"};
  width: ${({ width }) => (width ? width : "auto")};

  ${({ heroBanner }) =>
    heroBanner &&
    `
    color: ${white.o100};
    text-align: center;
    max-width: 600px;
    margin-bottom: 30px;
    transition: all 0.2s;
  `}

  &:hover {
    color: ${({ hcolor }) => hcolor && hcolor};
    transition: all 0.2s;
  }

  ${media.tablet`
    margin: ${({ marginSm }) => marginSm && marginSm};
    text-align: ${({ alignSm }) => alignSm && alignSm};
      font-size: ${({ sizeSm }) => sizeSm && sizeSm};
  `}

  ${media.phone`
    margin: ${({ marginXs }) => marginXs && marginXs};
    font-size: ${({ sizeXs }) => sizeXs && sizeXs};
    font-weight: ${({ weightXs }) => weightXs && weightXs};
    text-align: ${({ alignXs }) => alignXs && alignXs};
  `}
`

export default Text
