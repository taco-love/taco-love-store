import styled from "styled-components"
import { clickable } from "styles/partials"
import { primary, white } from "styles/colors"

export const Wrapper = styled.div`
  ${clickable}
 
  ${({ location, to, navLink, underlineAdditions }) =>
    navLink &&
    (location === to ||
      location === `${to}/` ||
      underlineAdditions.includes(location)) &&
    `
      border-bottom: 3px solid;
  `}
   border-color: ${({ isTop }) => (isTop ? white.o100 : primary.o100)};
  ${({ navLink }) =>
    navLink &&
    `
    margin-left: 20px;
    padding: 3px;
  `};
`
