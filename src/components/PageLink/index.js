import React from "react"
import PropTypes from "prop-types"
import { Link as GatsbyLink } from "gatsby"
import { Location } from "@reach/router"
import { Wrapper } from "./styles"

const PageLink = ({
  children,
  to,
  navLink,
  isWhite,
  underlineAdditions,
  replace,
  onClick,
  isTop,
  isDivLink,
}) => {
  if (isDivLink) {
    return (
      <Location>
        {({ location }) => (
          <GatsbyLink to={to} replace={replace}>
            <Wrapper
              location={location.pathname}
              to={to}
              underlineAdditions={underlineAdditions}
              navLink={navLink}
              isWhite={isWhite}
              onClick={onClick}
              isTop={isTop}
            >
              {children}
            </Wrapper>
          </GatsbyLink>
        )}
      </Location>
    )
  } else {
    return (
      <Location>
        {({ location }) => (
          <Wrapper
            location={location.pathname}
            to={to}
            underlineAdditions={underlineAdditions}
            navLink={navLink}
            isWhite={isWhite}
            onClick={onClick}
            isTop={isTop}
          >
            <GatsbyLink to={to} replace={replace}>
              {children}
            </GatsbyLink>
          </Wrapper>
        )}
      </Location>
    )
  }
}

PageLink.defaultProps = {
  underlineAdditions: [],
}

PageLink.propTypes = {
  onClick: PropTypes.func,
  children: PropTypes.node,
  to: PropTypes.string,
  navLink: PropTypes.bool,
  isWhite: PropTypes.bool,
  underlineAdditions: PropTypes.array,
  replace: PropTypes.bool,
  isTop: PropTypes.bool,
}

export default PageLink
