import React from "react"
import PropTypes from "prop-types"
import styled from "styled-components"
import { primary } from "styles/colors"
const Link = styled.a`
  transition: all 0.1s;
  &:hover {
    text-decoration: underline;
    color: ${primary.o100};
  }
  text-decoration: ${({ noUnderline }) =>
    noUnderline ? "none" : "underline"} !important;
`

const ExternalLink = ({ children, to, ohColor, ...restProps }) => {
  return (
    <Link
      target="_blank"
      rel="noopener noreferrer"
      href={to}
      ohColor={ohColor}
      {...restProps}
    >
      {children}
    </Link>
  )
}

ExternalLink.propTypes = {
  children: PropTypes.node,
  to: PropTypes.string,
  navLink: PropTypes.bool,
  isWhite: PropTypes.bool,
  underlineAdditions: PropTypes.array,
  replace: PropTypes.bool,
  ohColor: PropTypes.string,
}

export default ExternalLink
