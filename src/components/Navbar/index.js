import React from "react"
import PropTypes from "prop-types"
import styled from "styled-components"
import { Flex } from "reflexbox"
import { Link } from "gatsby"
import { Icon, Button } from "semantic-ui-react"
import FadeIn from "react-fade-in"

const Burger = styled(Icon)`
  &:hover {
    cursor: pointer;
  }
`
const Container = styled(Flex)`
  height: 60px;
  width: 100%;
  justify-content: space-between;
  padding: 10px;
  z-index: 5000;
  top: 0;
  left: 0;
  justify-content: center;
  && {
    background-color: ${({ bgColor }) => (bgColor ? bgColor : "none")};
    position: fixed;
    ${({ customCss }) => customCss}
  }
`

const MobileMenu = styled.div`
  min-height: 100vh;
  background-color: white;
  position: fixed;
  width: 100%;
  top: 0;
  z-index: 50000;
`

const MobileNavList = styled.div`
  font-size: 40px;
`

const MobileNavItem = styled.div`
  margin-bottom: 20px;
`
class Navbar extends React.Component {
  state = {
    mobileMenuOpen: false,
  }
  render() {
    const { LeftComponent, RightComponent, ...restProps } = this.props
    const { mobileMenuOpen } = this.state

    return (
      <>
        <Container
          alignItems="center"
          justifyContent="space-between"
          {...restProps}
        >
          <div style={{ height: "100%" }}>
            <Link to="/"></Link>
          </div>
          {LeftComponent}
          <>
            <Burger
              onClick={() => this.setState({ mobileMenuOpen: true })}
              name="bars"
              size="large"
            />
          </>
        </Container>
        {mobileMenuOpen && (
          <MobileMenu>
            <Flex p="10px" justifyContent="flex-end" style={{ height: "60px" }}>
              <>
                <Burger
                  onClick={() => this.setState({ mobileMenuOpen: false })}
                  name="close"
                  size="large"
                />
              </>
            </Flex>
            <Flex p="10px" justifyContent="flex-start">
              <MobileNavList>
                <FadeIn transitionDuration={1000}>
                  <MobileNavItem>
                    <Link to="/">Home</Link>
                  </MobileNavItem>
                  {/*<MobileNavItem>*/}
                  {/*  <a href="https://wolt.com/en/deu/berlin/restaurant/taco-love">*/}
                  {/*    Order Online*/}
                  {/*  </a>*/}
                  {/*</MobileNavItem>*/}
                  <MobileNavItem>
                    Get In Contact
                    <br />
                    <a
                      href="http://m.me/tacoloveberlin"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <Button icon size="large">
                        <Icon name="facebook messenger" />
                      </Button>
                    </a>
                  </MobileNavItem>
                </FadeIn>
              </MobileNavList>
            </Flex>
          </MobileMenu>
        )}
      </>
    )
  }
}

Navbar.propTypes = {
  LeftComponent: PropTypes.node,
  RightComponent: PropTypes.node,
  title: PropTypes.string,
}

export default Navbar
