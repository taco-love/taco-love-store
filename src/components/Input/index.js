import React from "react"
import PropTypes from "prop-types"
import styled from "styled-components"
import { teal, green } from "styles/colors"
import Margin from "components/Margin"

import { Flex } from "reflexbox"
const StyledInput = styled.input`
  height: 45px;
  border: 0;
  ${({ valid }) => {
    if (valid) {
      return `border-bottom: 2px solid ${green};`
    } else {
      return `border-bottom: 2px solid rgba(0,0,0,0.2);`
    }
  }}

  width: 100%;
  padding-left: 10px;
  outline: 0;

  ~ .border {
    position: absolute;
    opacity: 0;
    bottom: 0;
    left: 0;
    width: 0;
    height: 1px;
    border: 1px solid ${teal}!important;
  }

  &:focus ~ .border {
    opacity: 1;
    width: 100%;
    transition: 0.5s;
  }
`

const Icon = styled.div`
  color: ${teal};
`
const Input = ({ iconComponent, ...props }) => {
  return (
    <Margin margin="0px 0px 15px 0px">
      <Flex alignItems="center">
        {iconComponent && <Icon>{iconComponent}</Icon>}
        <div style={{ position: "relative", width: "100%" }}>
          <StyledInput {...props} />
          <span className="border"></span>
        </div>
      </Flex>
    </Margin>
  )
}

Input.propTypes = {
  iconComponent: PropTypes.node,
}
export default Input
