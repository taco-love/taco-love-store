import styled from "styled-components"

import media from "styles/media"
const CenteredContainer = styled.div`
  padding: ${({ padding }) => padding || "20px"};
  width: 80%;
  max-width: 700px;
  margin: 0 auto;
  ${media.tablet`
   
      `}

  ${media.phone`
    padding: 0;
    width:100%;
  `}
`
export default CenteredContainer
