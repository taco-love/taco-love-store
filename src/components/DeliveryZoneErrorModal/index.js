import { Button, Icon, Modal } from "semantic-ui-react"
import PropTypes from "prop-types"
import GoogleMapReact from "google-map-react"
import { getStoreCoordinates, getDeliveryRadius } from "entities/Store"
import React from "react"
import navigate from "gatsby"

const DeliveryZoneErrorModal = ({
  open,
  onClose,
  markerPosition: { latitude, longitude },
}) => {
  return (
    <Modal open={open} closeOnEscape closeIcon onClose={onClose}>
      <Modal.Header>
        <Icon name="exclamation circle" color="red" /> Outside Delivery Zone
      </Modal.Header>
      <Modal.Content>
        Sorry, your address is currently outside of our delivery zone. You can
        view our delivery zone on the map below.
        <div style={{ height: "200px" }}>
          <GoogleMapReact
            bootstrapURLKeys={{
              key: "AIzaSyBQC4OboUCV6pf5Ky_146vw9PNNb5nMfkc",
            }}
            defaultCenter={{
              lat: getStoreCoordinates().latitude,
              lng: getStoreCoordinates().longitude,
            }}
            defaultZoom={12}
            onGoogleApiLoaded={({ map, maps }) => {
              new maps.Circle({
                strokeColor: "#fbbd08",
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: "#fbbd08",
                fillOpacity: 0.4,
                map,
                center: {
                  lat: getStoreCoordinates().latitude,
                  lng: getStoreCoordinates().longitude,
                },
                radius: getDeliveryRadius(),
              })
            }}
          ></GoogleMapReact>
        </div>
      </Modal.Content>
      <Modal.Content style={{ marginLeft: "0px" }}>
        <Button
          color="yellow"
          fluid
          icon="bicycle"
          labelPosition="right"
          content="Change delivery method"
          onClick={() => {
            navigate("/delivery/method")
          }}
        />
        <br />
        <Button
          fluid
          positive
          icon="home"
          labelPosition="right"
          content="Use a different address"
          onClick={() => {
            this.setState({
              errorModalOpen: false,
            })
          }}
        />
      </Modal.Content>
    </Modal>
  )
}

DeliveryZoneErrorModal.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
  markerPosition: PropTypes.object,
}

export default DeliveryZoneErrorModal
