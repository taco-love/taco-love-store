import styled from "styled-components"
import { Flex } from "reflexbox"
import { white, black } from "styles/colors"
import media from "styles/media"

export const Container = styled(Flex)`
  align-items: center;
  flex-direction: column;
  width: 100%;
  padding-top: 100px;
  background-color: black;
  flex-wrap: wrap;
  ${media.tablet`
    padding-top:50px;
  `}

  ${media.phone`
    padding-top:20px;
  `}
`

export const PrivacyBar = styled(Flex)`
  justify-content: center;
  align-items: center;
  height: 72px;
  width: 100%;
  background-color: ${white.o30};
`

export const Margins = styled(Flex)`
  width: 1152px;
  justify-content: space-between;
  margin-bottom: 100px;

  ${media.tablet`
    width: 100%;
    margin-bottom: 20px;
    padding: 20px;
  `}
`

export const Column = styled(Flex)`
  width: 264px;
  flex-direction: column;

  ${media.tablet`
       flex-wrap: wrap;
         width: 300px;
  `}
`

export const MarginColumn = styled(Column)`
  margin-bottom: 40px;
`
export const Logo = styled.img`
  width: 64px;
  height: 64px;
`

export const IconWrapper = styled(Flex)`
  width: 150px;
  justify-content: space-between;
`

export const LogoWrapper = styled.div`
  ${media.phone`
    width: 100%;
    margin-bottom: 40px;
  `}
`
