import React from "react"
import { Flex } from "reflexbox"
import Text from "components/Text"
import { Link } from "gatsby"
import ExternalLink from "components/ExternalLink"
import LogoColored from "images/logos/logo-mark.png"
import { white, primary } from "styles/colors"
import { Button as Btn, Icon } from "semantic-ui-react"
import PageLink from "components/PageLink"
import {
  LogoWrapper,
  Container,
  PrivacyBar,
  Margins,
  Column,
  MarginColumn,
  IconWrapper,
  Logo,
} from "./styles"

const Footer = () => (
  <Container style={{ color: "white" }}>
    <Margins flexWrap="wrap">
      <LogoWrapper>
        <Logo src={LogoColored} />
      </LogoWrapper>
      <Flex flexWrap="wrap">
        <MarginColumn mb={3}>
          <Text size="16px" weight="600" margin="0 0 20px 0">
            Say hello
          </Text>
          <Text spacing="0.25" margin="0 0 26px 0">
            <ExternalLink
              to={"mailto:hello@tacoloveberlin.com"}
              ohColor={primary.o100}
            >
              hello@tacoloveberlin.com
            </ExternalLink>
          </Text>
          <IconWrapper>
            <ExternalLink
              noUnderline
              to={"https://instagram.com/tacoloveberlin"}
            >
              <Icon
                name="instagram"
                color={white.o30}
                hcolor={primary.o100}
                size="large"
              />
            </ExternalLink>
            <ExternalLink
              to={"https://www.facebook.com/tacoloveberlin"}
              noUnderline
            >
              <Icon
                name="facebook"
                color={white.o30}
                hcolor={primary.o100}
                size="large"
              />
            </ExternalLink>
          </IconWrapper>
        </MarginColumn>
        <Column>
          {/*<Text color={white.o30} size="16px" weight="600" margin="0 0 20px 0">*/}
          {/*  Sitemap*/}
          {/*</Text>*/}
          {/*<PageLink to={"/shop"}>*/}
          {/*  <Text*/}
          {/*    hcolor={primary.o100}*/}
          {/*    color={white.o100}*/}
          {/*    spacing="0.25"*/}
          {/*    margin="0 0 20px 0"*/}
          {/*  >*/}
          {/*    Order*/}
          {/*  </Text>*/}
          {/*</PageLink>*/}
          {/*<PageLink to={'/'}>*/}
          {/*  <Text*/}
          {/*    hcolor={primary.o100}*/}
          {/*    color={white.o100}*/}
          {/*    spacing="0.25"*/}
          {/*    margin="0 0 20px 0"*/}
          {/*  >*/}
          {/*    Listen to the playlist*/}
          {/*  </Text>*/}
          {/*</PageLink>*/}
        </Column>
      </Flex>
    </Margins>
    <PrivacyBar>
      <Text size="14px" spacing="0.25">
        © Taco Love 2020&nbsp;&nbsp;·&nbsp;&nbsp;
      </Text>
      <a href target="blank" href="/impressum">
        <Text size="14px" spacing="0.25">
          Impressum
        </Text>
      </a>
    </PrivacyBar>
  </Container>
)

export default Footer
