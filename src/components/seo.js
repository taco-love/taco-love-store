import React from "react"
import Helmet from "react-helmet"
import PropTypes from "prop-types"
import ogImage from "images/taco-love-og.jpg"
const Seo = ({ ogTitle, title, url, description, image }) => (
  <Helmet>
    <meta property="og:title" content={ogTitle} />
    <meta property="og:site_name" content="Taco Love" />
    <meta property="og:url" content={url} />
    <meta property="og:description" content={description} />
    <meta property="og:type" content="website" />
    <meta
      property="og:image"
      content={`https://tacoloveberlin.com${ogImage}`}
    />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@TacoLove" />
    <meta name="twitter:title" content={ogTitle} />
    <meta name="twitter:description" content={description} />
    <title>{title}</title>
  </Helmet>
)

Seo.propTypes = {
  ogTitle: PropTypes.string,
  title: PropTypes.string,
  url: PropTypes.string,
  description: PropTypes.string,
  image: PropTypes.string,
}

export default Seo
