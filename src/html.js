import React from "react"
import PropTypes from "prop-types"

export default function HTML(props) {
  return (
    <html {...props.htmlAttributes}>
      <head>
        <meta charSet="utf-8" />
        <meta httpEquiv="x-ua-compatible" content="ie=edge" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
        <meta property="og:title" content="Taco Love" />
        <meta
          property="og:description"
          content="Homemade corn tortillas wrapped around a healthy, hearty, fusion of international ingredients, delivered right to your hands. Made with love in Berlin."
        />

        <meta property="og:url" content="https://tacoloveberlin.com" />
        <meta
          property="og:image"
          content="https://tacoloveberlin.com/taco-love-og.jpg"
        />
        {props.headComponents}
      </head>
      <body {...props.bodyAttributes}>
        {props.preBodyComponents}
        <div
          key={`body`}
          id="___gatsby"
          dangerouslySetInnerHTML={{ __html: props.body }}
        />
        {props.postBodyComponents}
        <script
          src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQC4OboUCV6pf5Ky_146vw9PNNb5nMfkc&libraries=places"
          async
          defer
        ></script>
      </body>
    </html>
  )
}

HTML.propTypes = {
  htmlAttributes: PropTypes.object,
  headComponents: PropTypes.array,
  bodyAttributes: PropTypes.object,
  preBodyComponents: PropTypes.array,
  body: PropTypes.string,
  postBodyComponents: PropTypes.array,
}
