import Dexie from "dexie"

const db = new Dexie("TacoloveDb")
db.version(1).stores({
  basket: "items",
})

export default db
