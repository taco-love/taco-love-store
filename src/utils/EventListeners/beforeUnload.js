const beforeUnload = event => {
  {
    // Cancel the event as stated by the standard.
    event.preventDefault()
    // Chrome requires returnValue to be set.
    event.returnValue = "Are you sure you want to leave?"
  }
}

export default beforeUnload
