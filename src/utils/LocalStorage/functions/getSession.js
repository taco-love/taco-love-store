const getSession = key => {
  if (typeof window !== "undefined" && window) {
    return localStorage.getItem(key)
  }
}

export default getSession
