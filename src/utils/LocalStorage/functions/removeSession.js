const removeSession = key => {
  if (typeof window !== "undefined" && window) {
    localStorage.removeItem(key)
  }
}

export default removeSession
