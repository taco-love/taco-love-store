import get from "./functions/get"
import set from "./functions/set"

export default { get, set }
