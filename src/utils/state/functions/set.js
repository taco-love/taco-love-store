import db from "utils/db"
import { tableName } from "../const"

const set = async (key, value) => {
  const state = await db[tableName].get({ key: key })
  if (state) {
    return db[tableName].update(state.id, { value })
  } else {
    return db[tableName].add({ key, value })
  }
}

export default set
