import db from "utils/db"
import { tableName } from "../const"

const get = async key => {
  const state = await db.appState.get({ key })
  return state
}

export default get
