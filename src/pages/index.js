import React from "react"
import { graphql } from "gatsby"
import Layout from "components/layout"
import PropTypes from "prop-types"
import SEO from "components/seo"
import "swiper/css/swiper.css"
import styled from "styled-components"
import { Flex } from "reflexbox"
import withCurrentBreakpoint from "hoc/withCurrentBreakpoint"
import GoogleMapReact from "google-map-react"
import { getCoordinates } from "entities/Store"
import Img from "gatsby-image"
import logoMark from "images/logos/logo-mark.png"
import media from "styles/media"
import Footer from "components/Footer"

const HeroBanner = styled(Flex)`
  width: 100%;
  position: fixed;
  z-index: -1;
  height: 600px;
  justify-content: center;
  align-items: center;
  color: white;
  font-size: 50px;
  text-shadow: 2px 2px 3px rgba(0, 0, 0, 0.8);
  top: 0;
  font-weight: 600;
  & > img {
    object-fit: cover !important;
    object-position: 0% 0% !important;
    font-family: "object-fit: cover !important; object-position: 0% 0% !important;";
  }

  @media screen and (max-width: 600px) {
    height: ${({ mobileHeight }) => mobileHeight};
  }
`

const BgImage = styled(Img)`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: ${({ height }) => height};
  z-index: -1;

  & > img {
    object-fit: cover !important;
    object-position: 0% 0% !important;
    font-family: "object-fit: cover !important; object-position: 0% 0% !important;";
  }

  @media screen and (max-width: 600px) {
    height: ${({ mobileHeight }) => mobileHeight};
  }
`
const SectionContainer = styled.div`
  padding: 50px;
  width: ${({ width }) => width || "100%"};
  max-width: 1200px;
  margin: 0 auto;

  ${media.tablet`
    padding:30px 15px;
  `}

  ${media.phone`


    padding:30px 15px;
  `}
`
const Header = styled.div`
  color: white;
  z-index: 1;
  font-weight: 800;
  text-align: centered;
  opacity: 0.9;
`

const Section = styled(Flex)`
  ${({ bgColor }) => bgColor && `background-color:${bgColor}`};
  width: 100%;
  font-size: 40px;
  z-index: 1;
  font-weight: 800;
  color: ${({ color }) => color || "white"};


${media.desktopLarge`
   font-size:50px;
  `}
  
  ${media.tablet`
    font-size:30px;
  `}
  ${media.phone`

    font-size:25px;
  `}
`

const DescriptionSection = styled(Section)`
  text-align: center;
  margin-top: 500px !important;
  font-family: "Josefin Slab";

  ${media.phone`
    margin-top:450px !important;
  `}
`

const SubHeader = styled.div`
  font-size: 30px;
`

class IndexPage extends React.Component {
  state = {
    selections: [],
    totalPrice: 0,
    totalQuantity: 0,
    color: "yellow",
    tacos: [],
  }

  constructor(props) {
    super(props)
  }
  componentDidMount = () => {
    const { data } = this.props
    const { latitude, longitude } = getCoordinates()
    this.getTacos()
    this.setState({ latitude, longitude })
  }

  getTacos = () => {
    const selections = []
    const { data } = this.props
    const {
      prismic: {
        allMenu_items: { edges },
      },
    } = data
    const tacos = []
    edges.forEach(edge => {
      const {
        node: {
          name,
          description,
          image,
          imageSharp: {
            childImageSharp: { fluid },
          },
        },
      } = edge
      const taco = {
        name: name[0].text,
        description: description[0].text,
        image,
        imageSharp: fluid,
      }
      tacos.push(taco)
    })

    this.setState({ tacos })
  }

  render() {
    const { latitude, longitude, tacos } = this.state
    const {
      currentBreakpoint,
      data: {
        HeroBanner: {
          childImageSharp: { fluid: HeroBannerFluid },
        },
        HeroLogo: {
          childImageSharp: { fluid: HeroLogoFluid },
        },
      },
    } = this.props

    const bgColors = ["#92a65b"]
    let tacoIndex = 1
    return (
      <Layout menuPadding="0" navTitle={"Taco Love"} padding="0px">
        Temporarily Unavailable
        {/*<HeroBanner flexWrap="wrap" mobileHeight="600px">*/}
        {/*  <BgImage fluid={HeroBannerFluid} height="600px" />*/}

        {/*  <div style={{ width: "300px", position: "absolute" }}>*/}
        {/*    <Img fluid={HeroLogoFluid} />*/}
        {/*  </div>*/}
        {/*</HeroBanner>*/}

        {/*<DescriptionSection bgColor="#fdbf27">*/}
        {/*  <SectionContainer>*/}
        {/*    <Header>*/}
        {/*      Berlin Tacos From the Heart.*/}
        {/*      <br />*/}
        {/*      <br />*/}
        {/*      <img src={logoMark} width="45px" style={{ margin: "0 auto" }} />*/}
        {/*      <br />*/}
        {/*      Homemade corn tortillas wrapped around a healthy, hearty, fusion*/}
        {/*      of international ingredients, delivered right to your hands.*/}
        {/*      <br />*/}
        {/*      <br />*/}
        {/*      <br />*/}
        {/*      Plus salsa...lots and lots of salsa.*/}
        {/*    </Header>*/}
        {/*  </SectionContainer>*/}
        {/*</DescriptionSection>*/}
        {/*{tacos.map(taco => {*/}
        {/*  const multiplier = 10*/}
        {/*  const red = 146 + (tacoIndex % 2 === 1 ? multiplier : 0)*/}
        {/*  const green = 166 + (tacoIndex % 2 === 1 ? multiplier : 0)*/}
        {/*  const blue = 91*/}
        {/*  const color = `rgba(${red}, ${green}, ${blue})`*/}
        {/*  tacoIndex++*/}
        {/*  return (*/}
        {/*    <Section key={taco.name} bgColor={color} flexWrap="wrap">*/}
        {/*      <div*/}
        {/*        style={{*/}
        {/*          width: currentBreakpoint !== "phone" ? "50%" : "100%",*/}
        {/*        }}*/}
        {/*      >*/}
        {/*        <img src={taco.image.url} width="100%" />*/}
        {/*      </div>*/}
        {/*      <SectionContainer*/}
        {/*        width={currentBreakpoint !== "phone" ? "50%" : "100%"}*/}
        {/*      >*/}
        {/*        <div>{taco.name} </div>*/}
        {/*        <div style={{ fontWeight: 300, opacity: 0.8 }}>*/}
        {/*          {taco.description}*/}
        {/*        </div>*/}
        {/*      </SectionContainer>*/}
        {/*    </Section>*/}
        {/*  )*/}
        {/*})}*/}

        {/*<Section bgColor="#fdbf27">*/}
        {/*  <SectionContainer>*/}
        {/*    <div> We are moving! More news coming very soon :)</div>*/}
        {/*    <div>For now, you can find us at Knaackstraße 76, 10435 Berlin.</div>*/}
        {/*    <div>Saturday-Sunday 1pm-9pm</div>*/}
        {/*  </SectionContainer>*/}
        {/*</Section>*/}
        {/*/!*<Section>*!/*/}
        {/*/!*  <div style={{ width: "100%", height: "300px" }}>*!/*/}
        {/*/!*    <GoogleMapReact*!/*/}
        {/*/!*      bootstrapURLKeys={{*!/*/}
        {/*/!*        key: "AIzaSyBQC4OboUCV6pf5Ky_146vw9PNNb5nMfkc",*!/*/}
        {/*/!*      }}*!/*/}
        {/*/!*      defaultCenter={{ lat: latitude, lng: longitude }}*!/*/}
        {/*/!*      defaultZoom={15}*!/*/}
        {/*/!*      options={{*!/*/}
        {/*/!*        scrollwheel: false,*!/*/}
        {/*/!*      }}*!/*/}
        {/*/!*      yesIWantToUseGoogleMapApiInternals={true}*!/*/}
        {/*/!*      onGoogleApiLoaded={({ map, maps }) => {*!/*/}
        {/*/!*        new maps.Marker({*!/*/}
        {/*/!*          position: { lat: latitude, lng: longitude },*!/*/}
        {/*/!*          map: map,*!/*/}
        {/*/!*          title: "Location",*!/*/}
        {/*/!*        })*!/*/}
        {/*/!*      }}*!/*/}
        {/*/!*    ></GoogleMapReact>*!/*/}
        {/*/!*  </div>*!/*/}
        {/*/!*</Section>*!/*/}
        {/*<Footer />*/}
      </Layout>
    )
  }
}
export const query = graphql`
  {
    prismic {
      allMenu_items {
        edges {
          node {
            name
            description
            image
            price
            imageSharp {
              childImageSharp {
                fluid(maxWidth: 1000) {
                  base64
                  aspectRatio
                  src
                  srcSet
                  sizes
                }
              }
            }
            _linkType
          }
        }
      }
    }
    HeroBanner: file(relativePath: { eq: "tacos.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 1000) {
          base64
          aspectRatio
          src
          srcSet
          sizes
        }
      }
    }
    HeroLogo: file(relativePath: { eq: "logos/logo-white-mark-text.png" }) {
      childImageSharp {
        fluid(maxWidth: 300) {
          base64
          aspectRatio
          src
          srcSet
          sizes
        }
      }
    }
  }
`

IndexPage.propTypes = {
  data: PropTypes.object,
  currentBreakpoint: PropTypes.string,
}

export default withCurrentBreakpoint(IndexPage)
