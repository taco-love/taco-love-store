import React from "react"
import { Link, navigate, graphql } from "gatsby"
import Layout from "components/layout"
import PropTypes from "prop-types"
import SEO from "components/seo"
import Swiper from "react-id-swiper"
import "swiper/css/swiper.css"
import {
  Button,
  Label,
  Card,
  Dimmer,
  Loader,
  Modal,
  Icon,
} from "semantic-ui-react"
import media from "styles/media"
import styled from "styled-components"
import { Flex } from "reflexbox"
import FixedBottomButton from "components/FixedBottomButton"
import { getOrder, addItemToOrder, removeItemFromOrder } from "entities/Order"
import RRS from "react-responsive-select"
import "react-responsive-select/dist/ReactResponsiveSelect.css"
import FadeIn from "react-fade-in"
import { fetchInfo, getNotification, isStoreOpen } from "entities/Store"
import beforeUnload from "utils/EventListeners/beforeUnload"
const Container = styled(Flex)`
  align-items: center;
  justify-content: center;
  padding: 8px;
  text-align: center;
`
import withCurrentBreakpoint from "hoc/withCurrentBreakpoint"
import getMenu from "entities/Store/functions/getMenu"
import GoogleMapReact from "google-map-react"

export const Slide = styled.div`
  margin: 10px 12px 0;
  ${({ teamMember }) => teamMember && `max-width: 360px;`}
  font-size:25px;
  ${media.tablet`
    margin: 10px 0;
  `}
`

export const Description = styled.div`
  margin: 5px 0 0px;
  font-size: 16px;
  opacity: 0.75;
`
export const CarouselImg = styled.img`
  overflow: hidden;
  display: block;
  max-width: 300px;
  width: 100%;
`

export const TeamName = styled.h4`
  line-height: 28px;
  color: black;
  margin: 0;
`

export const StyledCard = styled(Card)`
  &&&& {
    ${({ highlighted }) =>
      highlighted ? "border: 5px solid #fbbd08" : "border: 5px solid white"};
    transition: 0.5s all;
  }
`

const Price = styled.div`
  font-weight: 800;
  color: rgba(0, 0, 0, 0.8);
`

const params = {
  slidesPerView: "auto",
  centeredSlides: true,
  spaceBetween: 30,
  loop: true,
  shouldSwiperUpdate: true,
  rebuildOnUpdate: true,
}

const ImageContainer = styled.div`
  &:hover {
    cursor: pointer;
  }

  margin-bottom: 10px;
`

const Quantity = styled(Label)`
  padding: 0.5em;
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  bottom: 5px;
  font-size: 15px;
  right: 5px;
  background-color: #fbbd08;
  color: white;
  border-radius: 50px;
  box-shadow: 0 6px 10px 0 rgba(0, 0, 0, 0.5);
`

const PositionedButton = styled(Button)`
    position: absolute;

    &&&& {background-color:white;    margin:0px; border-radius 0px;}
    
    ${({ top, left, bottom, right }) => {
      let css = ""
      if (top) {
        css += `top:${top};`
      }
      if (right) {
        css += `right:${right};`
      }
      if (left) {
        css += `left:${left};`
      }
      if (bottom) {
        css += `bottom:${bottom};`
      }
      return css
    }}
    transition: 0.5s all;
 &&& { box-shadow: 0 6px 10px 0 rgba(0,0,0,0.4)};
 &&&:active {
    box-shadow:none;
 }

    
    
`

class IndexPage extends React.Component {
  state = {
    selections: [],
    totalPrice: 0,
    totalQuantity: 0,
    color: "yellow",
    currentSelection: null,
    loading: true,
    notificationModalOpen: false,
    notification: {},
    closedShopModal: false,
  }

  componentDidMount = async () => {
    const { data } = this.props
    const [notification] = await Promise.all([
      getNotification(),
      fetchInfo(),
      this.buildSelectionSections(),
    ])

    const order = getOrder()
    this.setState({
      totalPrice: order.totalPrice,
      totalQuantity: order.totalQuantity,
      loading: false,
      notificationModalOpen: notification ? true : false,
      closedShopModal: !isStoreOpen(),
      notification,
    })
  }

  buildSelectionSections = async () => {
    const selections = await getMenu()

    const order = getOrder()
    const orderItems = order.items
    orderItems.forEach(item => {
      selections.forEach(selection => {
        const orderItem = selection.items.find(
          selectionItem => selectionItem.id === item.id
        )
        if (orderItem) orderItem.quantity = item.quantity
      })
    })

    this.setState({ selections, currentSelection: "All Menu Items" })
  }

  addToOrder = selectionItem => {
    const { selections, totalPrice, totalQuantity } = this.state
    selections.forEach(selection => {
      selection.items.forEach(item => {
        if (item.id == selectionItem.id) {
          item.quantity++
        }
      })
    })
    addItemToOrder(selectionItem)

    this.setState({
      totalPrice: totalPrice + selectionItem.price,
      totalQuantity: totalQuantity + 1,
      selections,
    })

    this.flashButtonColor()
  }

  flashButtonColor = color => {
    this.setState({ color: color || "green" }, () => {
      setTimeout(() => this.setState({ color: "yellow" }), 200)
    })
  }
  removeFromOrder = selectionItem => {
    const { selections, totalPrice, totalQuantity } = this.state

    selections.forEach(selection => {
      selection.items.forEach(item => {
        if (item.id == selectionItem.id) {
          item.quantity--
        }
      })
    })

    removeItemFromOrder(selectionItem)
    this.setState({
      totalPrice: totalPrice - selectionItem.price,
      totalQuantity: totalQuantity - 1,
      selections,
    })

    this.flashButtonColor("red")
  }

  renderSwiperSelection = selection => {
    let memberElements = []
    selection.map(val => {
      memberElements = [
        ...memberElements,
        <Slide key={val.id}>
          <StyledCard highlighted={val.quantity > 0}>
            <ImageContainer style={{ position: "relative" }}>
              <CarouselImg src={val.image} />
              {val.quantity > 0 && (
                <Quantity color="green">{val.quantity}</Quantity>
              )}
              <PositionedButton
                right="0px"
                top="0px"
                icon="plus"
                onClick={() => this.addToOrder(val)}
                size="large"
              />
              {val.quantity > 0 && (
                <PositionedButton
                  left="0px"
                  top="0px"
                  icon="minus"
                  onClick={() => this.removeFromOrder(val)}
                  size="large"
                />
              )}
            </ImageContainer>
            <div style={{ padding: "10px", minHeight: "150px" }}>
              <Flex justifyContent="space-between">
                <TeamName>{val.name}</TeamName> <Price>€{val.price}</Price>
              </Flex>
              <Description>{val.description}</Description>
            </div>
          </StyledCard>
        </Slide>,
      ]
    })
    return memberElements
  }

  transitionSelection = () => {
    const { selection } = this.state
    this.setState({ selection: false }, () => this.setState({ selection }))
  }

  DropdownExampleSearchSelection = () => {
    const { selections, currentSelection, swiper } = this.state

    const self = this

    const onChange = newValue => {
      if (newValue.altered) {
        try {
          swiper.slideTo(0)
        } catch (e) {
          //do nothing
        }
        this.setState({ currentSelection: [] }, () => {
          this.setState({ currentSelection: newValue.value })
        })
      }
    }
    const onSubmit = () => {}

    const CaretIcon = () => (
      <svg
        className="caret-icon"
        x="0px"
        y="0px"
        width="11.848px"
        height="6.338px"
        viewBox="351.584 2118.292 11.848 6.338"
      >
        <g>
          <path d="M363.311,2118.414c-0.164-0.163-0.429-0.163-0.592,0l-5.205,5.216l-5.215-5.216c-0.163-0.163-0.429-0.163-0.592,0s-0.163,0.429,0,0.592l5.501,5.501c0.082,0.082,0.184,0.123,0.296,0.123c0.103,0,0.215-0.041,0.296-0.123l5.501-5.501C363.474,2118.843,363.474,2118.577,363.311,2118.414L363.311,2118.414z" />
        </g>
      </svg>
    )

    const options = selections.map(selection => ({
      text: selection.title,
      value: selection.title,
      markup: <span>{selection.title}</span>,
    }))

    if (options.length === 0) return null
    const Form = () => (
      <form style={{ minWidth: "50px" }}>
        <div style={{ minWidth: "200px" }}>
          <RRS
            name="make"
            options={options}
            selectedValue={currentSelection}
            onSubmit={onSubmit}
            onChange={onChange}
            caretIcon={<CaretIcon />}
          />
        </div>
      </form>
    )

    return Form()
  }

  render() {
    const {
      selections,
      totalPrice,
      totalQuantity,
      currentSelection,
      loading,
      notification,
      notificationModalOpen,
    } = this.state
    const { currentBreakpoint } = this.props

    const selection = selections.find(
      selection => selection.title === currentSelection
    )

    return (
      <>
        <Layout
          navBgColor="white"
          navLeftComponent={
            !notificationModalOpen
              ? this.DropdownExampleSearchSelection()
              : null
          }
          navTitle={"Taco Love Store"}
          padding="0px"
          gradient={true}
        >
          {loading && (
            <Dimmer active inverted>
              <Loader size="large">Loading</Loader>
            </Dimmer>
          )}

          {selection && (
            <FadeIn transitionDuration={500}>
              <>
                {currentBreakpoint === "phone" ? (
                  <Swiper
                    {...{
                      slidesPerView: 1.5,
                      spaceBetween: 20,
                      centeredSlides: true,
                      loop: false,
                      shouldSwiperUpdate: true,
                      getSwiper: swiper => this.setState({ swiper }),
                    }}
                  >
                    {this.renderSwiperSelection(selection.items)}
                  </Swiper>
                ) : (
                  <Container flexWrap="wrap">
                    {this.renderSwiperSelection(selection.items)}
                  </Container>
                )}
              </>
            </FadeIn>
          )}
          {!notificationModalOpen && (
            <FixedBottomButton
              disabled={totalQuantity <= 0}
              onClick={() => navigate("/order/selection-review")}
              color={this.state.color}
            >
              Continue (€{totalPrice})
              <Label color="green" floating>
                {totalQuantity}
              </Label>
            </FixedBottomButton>
          )}
        </Layout>
        <Modal
          open={this.state.notificationModalOpen}
          size="small"
          style={{ zIndex: 999999999 }}
        >
          <Modal.Header>{notification.title}</Modal.Header>
          <Modal.Content>
            {notification.content}
            <br />
            <br />
            <img width="100%" src={notification.image} />
          </Modal.Content>
          <Modal.Content style={{ marginLeft: "0px" }}>
            <Button
              fluid
              positive
              content="Continue"
              onClick={() => {
                this.setState({
                  notificationModalOpen: false,
                })
              }}
            />
          </Modal.Content>
        </Modal>

        <Modal
          open={this.state.closedShopModal}
          size="mini"
          style={{ zIndex: 999999999 }}
        >
          <Modal.Header>Sorry! We are closed right now</Modal.Header>
          <Modal.Content>
            We would love to get you your tacos, unfortunately our shop is
            closed for delivery right now. :(
          </Modal.Content>
          <Modal.Content style={{ marginLeft: "0px" }}>
            <Button
              fluid
              positive
              content="Back to Home"
              onClick={() => {
                navigate("/")
              }}
            />
          </Modal.Content>
        </Modal>
      </>
    )
  }
}
export const query = graphql`
  {
    prismic {
      allMenu_categorys {
        edges {
          node {
            category_name
            menu_items {
              menu_item {
                ... on PRISMIC_Menu_item {
                  name
                  price
                  description
                  image
                }
              }
            }
            _linkType
          }
        }
      }
      allMenu_items {
        edges {
          node {
            name
            description
            image
            price
            imageSharp {
              childImageSharp {
                fluid(maxWidth: 1000) {
                  base64
                  aspectRatio
                  src
                  srcSet
                  sizes
                }
              }
            }
            _linkType
          }
        }
      }
    }
  }
`

IndexPage.propTypes = {
  data: PropTypes.object,
  currentBreakpoint: PropTypes.string,
}

export default withCurrentBreakpoint(IndexPage)
