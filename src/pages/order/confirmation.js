import React from "react"
import { navigate, Link } from "gatsby"
import Layout from "components/layout"
import SEO from "components/seo"
import "swiper/css/swiper.css"

import {
  Button,
  Label,
  Segment,
  Input,
  Icon,
  Step,
  Progress,
} from "semantic-ui-react"
import styled from "styled-components"
import { Flex } from "reflexbox"
import {
  getOrder,
  addItemToOrder,
  removeItemFromOrder,
  resetOrder,
} from "entities/Order"
import FadeIn from "react-fade-in"
import GoogleMapReact from "google-map-react"
const Marker = styled.div`
  width: 10px;
  height: 10px;
  background-color: black;
`
const Centered = styled(Flex)`
  align-items: center;
  justify-content: center;
  height: 100vh;
  width: 100vw;
  padding: 8px;
  text-align: center;
`

const SegmentContainer = styled(Flex)`
  text-align: left;
`

const Description = styled.div`
  font-size: 12px;
  margin-top: 5px;
  opacity: 0.75;
`

const ExternalLink = styled.a`
  &&&&&&& {
    text-decoration: none !important;
  }
`

class IndexPage extends React.Component {
  state = {
    order: getOrder(),
    itemToRemove: {},
    deleteModalOpen: false,
  }

  componentDidMount = () => {
    resetOrder()
  }
  addQuantity = item => {
    addItemToOrder(item)
    this.setState({ order: getOrder() })
  }

  removeQuantity = item => {
    if (item.quantity === 1) {
      this.openConfirmRemoveModal(item)
    } else {
      this.removeItem(item)
    }
  }

  removeItem = item => {
    removeItemFromOrder(item)
    this.setState({ order: getOrder() })
  }

  openConfirmRemoveModal = item => {
    this.setState({
      itemToRemove: item,
      deleteModalOpen: true,
    })
  }

  isValid = () => {
    const { name, telephone } = this.state
    return name && telephone
  }

  render() {
    const {
      order: { deliveryLocation, id },
    } = this.state

    const { latitude, longitude } = deliveryLocation || {}
    return (
      <Layout navTitle={"Taco Love Store"}>
        <h1>Order #{id}</h1>
        <FadeIn>
          <Progress
            style={{ marginBottom: "10px" }}
            percent="10"
            active
            color="yellow"
          />
          <Step.Group ordered fluid>
            <Step completed>
              <Step.Content>
                <Step.Title>Order Placed</Step.Title>
                <Step.Description>
                  Your order has been recieved.
                </Step.Description>
              </Step.Content>
            </Step>

            <Step disabled>
              <Step.Content>
                <Step.Title>Prepping</Step.Title>
                <Step.Description>
                  Your Tacos are being prepared for delivery{" "}
                </Step.Description>
              </Step.Content>
            </Step>

            <Step disabled>
              <Step.Content>
                <Step.Title>Delivering</Step.Title>
                <Step.Description>Your Tacos are on the way!</Step.Description>
              </Step.Content>
            </Step>
          </Step.Group>

          <Segment style={{ height: "200px" }}>
            <GoogleMapReact
              bootstrapURLKeys={{
                key: "AIzaSyBQC4OboUCV6pf5Ky_146vw9PNNb5nMfkc",
              }}
              defaultCenter={{ lat: latitude, lng: longitude }}
              defaultZoom={15}
            >
              <Marker
                lat={latitude}
                lng={longitude}
                text="My Marker"
                draggable={true}
              />
            </GoogleMapReact>
          </Segment>
          <Segment>
            <h1>
              We would love your feedback to help improve our Taco experience!
            </h1>
            <br />
            <ExternalLink
              href="https://www.surveymonkey.de/r/698CPSP"
              target="_blank"
              rel="noopener noreferrer"
            >
              <Button fluid color="yellow" size="large">
                Leave feedback
              </Button>
            </ExternalLink>
          </Segment>
        </FadeIn>
      </Layout>
    )
  }
}

export default IndexPage
