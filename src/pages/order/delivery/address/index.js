import React from "react"
import Layout from "components/layout"
import SEO from "components/seo"
import { navigate } from "gatsby"
import styled from "styled-components"
import { Flex } from "reflexbox"
import { Icon, Button, TextArea, Form, Modal } from "semantic-ui-react"
import {
  setOrderDeliveryAddress,
  getOrder,
  setDeliveryType,
  setDeliveryLocation,
} from "entities/Order"
import { DELIVERY_TYPES } from "entities/Order/const"
import Geocode from "react-geocode"
import Autocomplete from "react-google-autocomplete"
import {
  canDeliverToLocation,
  getDeliveryRadius,
  getCoordinates as getStoreCoordinates,
} from "entities/Store"
import FixedBottomButton from "components/FixedBottomButton"
import GoogleMapReact from "google-map-react"
import DeliveryZoneErrorModal from "components/DeliveryZoneErrorModal"
import CenteredContainer from "components/CenteredContainer"
import FadeIn from "react-fade-in"
import TopLeftButton from "components/TopLeftButton"
const StyledAutocomplete = styled(Autocomplete)`
    border:1px solid black;
    height:50px;
    padding: 5px;
    margin: 0;
    max-width: 100%;
    flex: 1 0 auto;
    outline: 0;
   
    text-align: left;
    line-height: 1.21428571em;
    padding: .67857143em 1em;
    background: #fff;
    border: 1px solid rgba(34,36,38,.15);
    color: rgba(0,0,0,.87);
    border-radius: .28571429rem;
    -webkit-transition: border-color .1s ease,-webkit-box-shadow .1s ease;
    -webkit-transition: box-shadow .1s ease,border-color .1s ease;
    transition: box-shadow .1s ease,border-color .1s ease;
    box-shadow: none;
    
    
       &&&& ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
  color: rgba(0,0,0,0.3);
  opacity: 1; /* Firefox */
  
    & :focus {
        border-color: #fbbd08;
    }
`
const Centered = styled(Flex)`
  align-items: center;
  justify-content: center;
  text-align: center;
`

const Container = styled(Flex)`
  align-items: center;
  justify-content: center;
  height: 90vh;
  padding: 8px;
`

const ConfirmButton = styled(Button)`
  position: fixed;
  bottom: 10px;
  width: 80vw;
`

const Header = styled.div`
  font-size: 25px;
  margin-bottom: 10px;
`

const StyledTextArea = styled(TextArea)`
  &&&& {
    margin-top: 20px;
    font-size: 16px;
  }
  &&&& ::placeholder {
    /* Chrome, Firefox, Opera, Safari 10.1+ */

    color: rgba(0, 0, 0, 0.3);
    opacity: 1; /* Firefox */
  }
`

const CloseIcon = styled(Icon)`
  &:hover {
    cursor: pointer;
  }
  position: absolute;
  right: 10px;
  top: 15px;
`
class IndexPage extends React.Component {
  state = {
    selected: "delivery",
    address: getOrder().deliveryAddress,
    notes: getOrder().deliveryNotes,
    selectedAddress: getOrder().deliveryAddress,
  }

  componentDidMount() {
    Geocode.setApiKey("AIzaSyBQC4OboUCV6pf5Ky_146vw9PNNb5nMfkc")
  }

  switchSelected = type => {
    this.setState({ selected: type })
  }

  submit = async () => {
    const { selectedAddress, deliveryNotes } = this.state
    const response = await Geocode.fromAddress(selectedAddress)
    const { lat, lng } = response.results[0].geometry.location
    this.state.location = { latitude: lat, longitude: lng }
    const canDeliver = canDeliverToLocation({ latitude: lat, longitude: lng })
    if (canDeliver) {
      setDeliveryLocation(lat, lng)
      setOrderDeliveryAddress(selectedAddress, deliveryNotes)
      setDeliveryType(DELIVERY_TYPES.deliveryAddress)
      navigate("/order/order-review")
    } else {
      this.setState({ errorModalOpen: true })
    }
  }

  render() {
    const { selected } = this.state
    return (
      <Layout padding="0px" menuPadding="0px">
        <TopLeftButton onClick={() => navigate("/order/delivery/method")}>
          Back
        </TopLeftButton>
        <CenteredContainer>
          <Container>
            <div style={{ width: "100%" }}>
              <FadeIn>
                <Header>Where are we delivering to?</Header>
                <div style={{ position: "relative" }}>
                  <StyledAutocomplete
                    value={this.state.address}
                    onChange={e =>
                      this.setState({
                        address: e.target.value,
                        selectedAddress: null,
                      })
                    }
                    style={{ width: "100%" }}
                    onPlaceSelected={place => {
                      const address = place.formatted_address
                      this.setState({ selectedAddress: address, address })
                    }}
                    placeholder="Enter your address"
                    types={["address"]}
                    componentRestrictions={{ country: "de" }}
                  />

                  <CloseIcon
                    color="grey"
                    style={{
                      position: "absolute",
                      right: "-10px",
                      top: "-5px",
                    }}
                    onClick={() =>
                      this.setState({ selectedAddress: null, address: "" })
                    }
                    name="close"
                  />
                </div>

                <Form>
                  <StyledTextArea
                    onChange={e =>
                      this.setState({ deliveryNotes: e.target.value })
                    }
                    placeholder="(Optional) Delivery instructions, e.g Floor, Back house, Doorbell name"
                  />
                </Form>
              </FadeIn>
            </div>
          </Container>
        </CenteredContainer>

        <FixedBottomButton
          disabled={!this.state.selectedAddress}
          onClick={this.submit}
          color="yellow"
        >
          Continue
        </FixedBottomButton>
        <Modal
          open={this.state.errorModalOpen}
          closeOnEscape
          closeIcon
          size="small"
          onClose={() => this.setState({ errorModalOpen: false })}
        >
          <Modal.Header>
            <Icon name="exclamation circle" color="red" /> Outside Delivery Zone
          </Modal.Header>
          <Modal.Content>
            Sorry, your address is currently outside of our delivery zone. You
            can view our delivery zone on the map below.
            <div style={{ height: "200px" }}>
              <GoogleMapReact
                bootstrapURLKeys={{
                  key: "AIzaSyBQC4OboUCV6pf5Ky_146vw9PNNb5nMfkc",
                }}
                defaultCenter={{
                  lat: getStoreCoordinates().latitude,
                  lng: getStoreCoordinates().longitude,
                }}
                defaultZoom={12}
                onGoogleApiLoaded={({ map, maps }) => {
                  new maps.Circle({
                    strokeColor: "#fbbd08",
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: "#fbbd08",
                    fillOpacity: 0.4,
                    map,
                    center: {
                      lat: getStoreCoordinates().latitude,
                      lng: getStoreCoordinates().longitude,
                    },
                    radius: getDeliveryRadius(),
                  })
                }}
              ></GoogleMapReact>
            </div>
          </Modal.Content>
          <Modal.Content style={{ marginLeft: "0px" }}>
            <Button
              color="yellow"
              fluid
              icon="bicycle"
              labelPosition="right"
              content="Change delivery method"
              onClick={() => {
                navigate("/order/delivery/method")
              }}
            />
            <br />
            <Button
              fluid
              positive
              icon="home"
              labelPosition="right"
              content="Use a different address"
              onClick={() => {
                this.setState({
                  errorModalOpen: false,
                })
              }}
            />
          </Modal.Content>
        </Modal>
      </Layout>
    )
  }
}

export default IndexPage
