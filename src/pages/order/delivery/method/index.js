import React from "react"
import Layout from "components/layout"
import SEO from "components/seo"
import { navigate } from "gatsby"
import styled from "styled-components"
import { Flex } from "reflexbox"
import { Icon, Segment } from "semantic-ui-react"
import FixedBottomButton from "components/FixedBottomButton"
import { DELIVERY_TYPES } from "entities/Order/const"
import { getOrder, setDeliveryType } from "entities/Order"
import CenteredContainer from "components/CenteredContainer"
import TopLeftButton from "components/TopLeftButton"
import FadeIn from "react-fade-in"
const Centered = styled(Flex)`
  align-items: center;
  justify-content: center;
  text-align: center;
`

const Container = styled(Flex)`
  align-items: center;
  justify-content: center;
  height: 100vh;
  padding: 8px;
`

const Selection = styled(Flex)`
  align-items: center;
  padding: 10px;
  text-align: left;
  justify-content: space-between;
  min-height: 100px;
  border-radius: 5px;
  border: 1px solid rgba(0, 0, 0, 0.6);
  width: 100%;
  & > i {
  }
  ${({ selected }) =>
    selected &&
    "color:black;border: 3px solid #fbbd08;background-color: rgba(251,189,8,0.3);"}
  ${({ selected }) =>
    !selected &&
    "color:rgba(0,0,0,0.6);border: 1px solid rgba(0,0,0,0.6);background-color: white"};

  &:hover {
    cursor: pointer;
    border: 3px solid #fbbd08;
  }
  transition: 0.3s all;
`

const SELECTION_TYPES = {
  delivery_address: "delivery_address",
  delivery_location: "delivery_location",
  pickup: "pickup",
}
class IndexPage extends React.Component {
  state = {
    selected: getOrder().deliveryType || DELIVERY_TYPES.deliveryAddress,
  }

  switchSelected = type => {
    this.setState({ selected: type })
  }

  navigateToNext = () => {
    const { selected } = this.state
    setDeliveryType(selected)
    if (selected === DELIVERY_TYPES.deliveryAddress) {
      navigate("/order/delivery/address")
    }

    if (selected === DELIVERY_TYPES.deliveryLocation) {
      navigate("/order/delivery/location")
    }

    if (selected === DELIVERY_TYPES.pickup) {
      navigate("/order/order-review")
    }
  }

  render() {
    const { selected } = this.state
    return (
      <Layout padding="0px" menuPadding="0px" navTitle={"Taco Love Store"}>
        <TopLeftButton onClick={() => navigate("/order/selection-review")}>
          Back
        </TopLeftButton>
        <CenteredContainer>
          <Container>
            <div style={{ width: "100%", maxWidth: "400px" }}>
              <FadeIn>
                <Selection
                  mb={3}
                  selected={selected === DELIVERY_TYPES.deliveryAddress}
                  onClick={() =>
                    this.switchSelected(DELIVERY_TYPES.deliveryAddress)
                  }
                >
                  <div style={{ display: "inherit" }}>
                    <div style={{ marginRight: "5px" }}>
                      <Icon name="bicycle" size="big" />
                    </div>
                    <Centered>Delivery to your address.</Centered>
                  </div>
                </Selection>
                <Selection
                  mb={3}
                  selected={selected === DELIVERY_TYPES.deliveryLocation}
                  onClick={() =>
                    this.switchSelected(DELIVERY_TYPES.deliveryLocation)
                  }
                >
                  <div style={{ display: "inherit" }}>
                    <div style={{ marginRight: "5px" }}>
                      <Icon name="map marker alternate" size="big" />
                    </div>
                    <Centered>Delivery to your gps location.</Centered>
                  </div>
                </Selection>
                <Selection
                  selected={selected === DELIVERY_TYPES.pickup}
                  onClick={() => this.switchSelected(DELIVERY_TYPES.pickup)}
                >
                  <div style={{ display: "inherit" }}>
                    <div style={{ marginRight: "5px" }}>
                      <Icon name="shopping basket" size="big" />
                    </div>
                    <Centered>
                      Pick up at our location
                      <br />
                    </Centered>
                  </div>
                </Selection>
              </FadeIn>
            </div>
          </Container>
          <FixedBottomButton onClick={this.navigateToNext} color="yellow">
            Continue
          </FixedBottomButton>
        </CenteredContainer>
      </Layout>
    )
  }
}

export default IndexPage
