import React from "react"
import Layout from "components/layout"
import SEO from "components/seo"
import { navigate, Link } from "gatsby"
import styled from "styled-components"
import { Flex } from "reflexbox"
import { Button, Dimmer, Icon, Message, Modal } from "semantic-ui-react"
import Geolocation from "react-geolocation"
import Autocomplete from "react-google-autocomplete"
import GoogleMapReact from "google-map-react"
import { Loader } from "semantic-ui-react"
import { DELIVERY_TYPES } from "entities/Order/const"
import { setDeliveryType, setDeliveryLocation } from "entities/Order"
import FixedBottomButton from "components/FixedBottomButton"
import {
  getDeliveryCenter,
  getDeliveryRadius,
  canDeliverToLocation,
  getInfo as getStoreInfo,
} from "entities/Store"
import locationIcon from "images/icons/location.png"
const Container = styled(Flex)`
  align-items: center;
  justify-content: center;
  height: 100vh;
  width: 100vw;
  padding: 8px;
`

const ConfirmButton = styled(Button)`
  position: fixed;
  bottom: 10px;
  width: 80vw;
`

const Marker = styled.div`
  width: 10px;
  height: 10px;
  border-radius: 20px;
  background-color: blue;
`
const Header = styled.div`
  font-size: 25px;
  margin-bottom: 10px;
`

class Demo extends React.Component {
  state = {
    loading: true,
    map: null,
    deliveryPosition: getDeliveryCenter(),
    deliveryCenter: getDeliveryCenter(),
  }

  canDeliverToLocation = () => {
    const { deliveryPosition } = this.state
    return canDeliverToLocation(deliveryPosition)
  }
  navigateToNext = () => {
    const { deliveryPosition } = this.state
    setDeliveryType(DELIVERY_TYPES.deliveryLocation)
    setDeliveryLocation(deliveryPosition.latitude, deliveryPosition.longitude)
    navigate("/order/order-review")
  }

  viewDeliveryArea = () => {
    const { map, marker } = this.state
    map.setCenter({
      lat: getDeliveryCenter().latitude,
      lng: getDeliveryCenter().longitude,
    })
    map.setZoom(14)
    marker.setPosition(map.getCenter())
  }

  moveMapToPos = ({ latitude, longitude }) => {
    const { map, marker } = this.state
    map.setCenter({ lat: latitude, lng: longitude })
    this.setState({ deliveryPosition: { latitude, longitude }, loading: false })
    marker.setPosition({ lat: latitude, lng: longitude })
  }

  closeGeolocationErrorModal = () => {
    this.setState({
      geolocationDisabled: false,
      geolocationNotSupported: false,
    })
  }
  render() {
    const self = this
    const { loading } = this.state
    const { latitude, longitude } = getDeliveryCenter()
    return (
      <>
        <div style={{ height: "100vh", width: "100%", position: "fixed" }}>
          <Button
            color="green"
            style={{ position: "absolute", top: 5, left: 5, zIndex: 1000 }}
            onClick={() => {
              navigate("/order/delivery/method")
            }}
          >
            Back
          </Button>
          <GoogleMapReact
            bootstrapURLKeys={{
              key: "AIzaSyBQC4OboUCV6pf5Ky_146vw9PNNb5nMfkc",
            }}
            defaultCenter={{ lat: latitude, lng: longitude }}
            defaultZoom={15}
            yesIWantToUseGoogleMapApiInternals={true}
            onGoogleApiLoaded={({ map, maps }) => {
              var marker = new maps.Marker({
                position: { lat: latitude, lng: longitude },
                map: map,
                animation: maps.Animation.DROP,
                title: "Location",
                zIndex: 1000,
              })
              if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(
                  function(position) {
                    self.setState({ loading: false })
                    const {
                      coords: { latitude, longitude },
                    } = position
                    const image = {
                      url: locationIcon,
                      anchor: new maps.Point(8, 8),
                    }
                    new maps.Circle({
                      strokeColor: "#189aff",
                      strokeOpacity: 0.3,
                      strokeWeight: 2,
                      fillColor: "#189aff",
                      fillOpacity: 0.3,
                      map,
                      center: { lat: latitude, lng: longitude },
                      zIndex: 888,
                      radius: 100,
                    })

                    var myloc = new maps.Marker({
                      clickable: false,
                      icon: image,
                      shadow: null,
                      zIndex: 999,
                      map,
                      position: { lat: latitude, lng: longitude },
                    })

                    self.moveMapToPos({ latitude, longitude })
                  },
                  err => {
                    self.setState({
                      loading: false,
                      geolocationDisabled: true,
                      geolocationNotSupported: false,
                    })
                  }
                )
              } else {
                self.setState({
                  loading: false,
                  geolocationNotSupported: true,
                  geolocationDisabled: false,
                })
              }

              new maps.Circle({
                strokeColor: "#fbbd08",
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: "#fbbd08",
                fillOpacity: 0.3,
                map,
                zIndex: 777,
                center: {
                  lat: getDeliveryCenter().latitude,
                  lng: getDeliveryCenter().longitude,
                },
                radius: getDeliveryRadius(),
              })

              maps.event.addListener(map, "dragend", function() {
                marker.setPosition(this.getCenter())
                self.setState({
                  deliveryPosition: {
                    latitude: this.getCenter().lat(),
                    longitude: this.getCenter().lng(),
                  },
                })
              })

              maps.event.addListener(map, "drag", function() {
                const pos = this.getCenter().lat()
                marker.setPosition({
                  lat: this.getCenter().lat() + 0.0002,
                  lng: this.getCenter().lng(),
                })
              })

              self.setState({
                map,
                marker,
                deliveryPosition: { latitude, longitude },
              })
            }}
          ></GoogleMapReact>
        </div>

        <FixedBottomButton
          disabled={
            loading || !latitude || !longitude || !this.canDeliverToLocation()
          }
          onClick={() => this.navigateToNext(latitude, longitude)}
          color="yellow"
        >
          Continue
        </FixedBottomButton>

        {!this.canDeliverToLocation() && (
          <Message
            style={{
              margin: "10px 10px",
              position: "absolute",
              top: 0,
              zIndex: 10000,
            }}
            negative
          >
            <Message.Header>Out of delivery zone</Message.Header>
            <p>
              Sorry, this is outside of our delivery zone. You can move your
              marker in <a onClick={this.viewDeliveryArea}>our deliver zone</a>,
              or{" "}
              <Link to="/order/delivery/method">
                select another delivery method
              </Link>
            </p>
          </Message>
        )}

        {loading && (
          <Dimmer active inverted>
            <Loader size="large">Loading</Loader>
          </Dimmer>
        )}

        <Modal
          open={
            this.state.geolocationDisabled || this.state.geolocationNotSupported
          }
          closeOnEscape
          closeIcon
          size="small"
          onClose={() => this.closeGeolocationErrorModal()}
        >
          <Modal.Header>
            <Icon name="exclamation circle" color="red" />
            {this.state.geolocationNotSupported && <>Browser not supported</>}
            {this.state.geolocationDisabled && <>Geolocation Disabled</>}
          </Modal.Header>
          <Modal.Content>
            {this.state.geolocationNotSupported && (
              <>
                Oops, looks like your browser doesn&apos;t support GPS location,
                we won&apos;t be able to pinpoint your exact location. However,
                you can still place a pin on the map within out delivery zone if
                you know where you are.
              </>
            )}
            {this.state.geolocationDisabled && (
              <>
                Oops, looks like you don&apos;t have geolocation enabled. You
                can enable it in your Privacy-&gt;Location Settings. If you
                continue, you can still place a pin on the map within out
                delivery zone if you know where you are.
              </>
            )}
          </Modal.Content>
          <Modal.Content style={{ marginLeft: "0px" }}>
            {this.state.geolocationDisabled && (
              <>
                <Button
                  color="teal"
                  fluid
                  icon="refresh"
                  labelPosition="right"
                  content="I have enabled location settings"
                  onClick={() => {
                    location.reload()
                  }}
                />
                <br />
              </>
            )}
            <Button
              color="yellow"
              fluid
              icon="bicycle"
              labelPosition="right"
              content="Change delivery method"
              onClick={() => {
                navigate("/order/delivery/method")
              }}
            />
            <br />
            <Button
              fluid
              positive
              icon="home"
              labelPosition="right"
              content="Continue anyways"
              onClick={this.closeGeolocationErrorModal}
            />
          </Modal.Content>
        </Modal>
      </>
    )
  }
}

export default Demo
