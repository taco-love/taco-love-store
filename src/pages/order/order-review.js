import React from "react"
import { navigate, Link } from "gatsby"
import Layout from "components/layout"
import SEO from "components/seo"
import "swiper/css/swiper.css"
import {
  Button,
  Label,
  Segment,
  Input,
  Icon,
  Dimmer,
  Loader,
} from "semantic-ui-react"
import styled from "styled-components"
import { Flex } from "reflexbox"
import { getOrder, submitOrder } from "entities/Order"
import { DELIVERY_TYPES } from "entities/Order/const"
import PaymentSelection from "components/PaymentSelection"
import GoogleMapReact from "google-map-react"
import CenteredContainer from "components/CenteredContainer"
import FadeIn from "react-fade-in"
import { setCustomerInfo } from "entities/Order"
import { getInfo } from "entities/Store"
const Marker = styled.div`
  width: 10px;
  height: 10px;
  background-color: black;
`

class IndexPage extends React.Component {
  state = {
    order: getOrder(),
    store: getInfo(),
  }

  isValid = () => {
    const { name, telephone } = this.state
    return name && telephone
  }

  renderDeliveryAddressInfo = () => {
    const {
      order: { deliveryAddress, deliveryNotes },
    } = this.state

    return (
      <>
        <p>Delivery</p>

        {deliveryAddress && (
          <>
            <p>
              <strong>Address:</strong> {deliveryAddress}
            </p>
          </>
        )}

        {deliveryNotes && (
          <>
            <p>
              <strong>Notes:</strong> {deliveryNotes}
            </p>
          </>
        )}

        <Label style={{ width: "max-content" }} color="green">
          <Icon name="bicycle" /> Free delivery
        </Label>
      </>
    )
  }

  renderDeliveryLocationInfo = () => {
    const {
      order: {
        deliveryLocation: { latitude, longitude },
      },
    } = this.state

    return (
      <>
        <p>Delivery</p>

        {longitude && latitude && (
          <div style={{ height: "100px" }}>
            <GoogleMapReact
              bootstrapURLKeys={{
                key: "AIzaSyBQC4OboUCV6pf5Ky_146vw9PNNb5nMfkc",
              }}
              defaultCenter={{ lat: latitude, lng: longitude }}
              defaultZoom={15}
              yesIWantToUseGoogleMapApiInternals={true}
              onGoogleApiLoaded={({ map, maps }) => {
                new maps.Circle({
                  strokeColor: "#FF0000",
                  strokeOpacity: 0.8,
                  strokeWeight: 2,
                  fillColor: "#FF0000",
                  fillOpacity: 0.1,
                  map,
                  center: { lat: 5, lng: 5 },
                  radius: 800,
                })

                new maps.Marker({
                  position: { lat: latitude, lng: longitude },
                  map: map,
                  title: "Location",
                })
              }}
            ></GoogleMapReact>
          </div>
        )}

        <br />
        <Label style={{ width: "max-content" }} color="green">
          <Icon name="bicycle" /> Free delivery
        </Label>
      </>
    )
  }

  renderPickupInfo = () => {
    const { store } = this.state
    const { latitude, longitude } = store.location
    return (
      <>
        <p>
          Pickup @ {store.address}
          <br />
          <a
            href={`https://maps.google.com?q=${encodeURI(store.address)}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            Open in maps
          </a>
        </p>

        <div style={{ height: "100px" }}>
          <GoogleMapReact
            bootstrapURLKeys={{
              key: "AIzaSyBQC4OboUCV6pf5Ky_146vw9PNNb5nMfkc",
            }}
            defaultCenter={{ lat: latitude, lng: longitude }}
            defaultZoom={15}
            yesIWantToUseGoogleMapApiInternals={true}
            onGoogleApiLoaded={({ map, maps }) => {
              new maps.Marker({
                position: { lat: latitude, lng: longitude },
                map: map,
                title: "Location",
              })
            }}
          ></GoogleMapReact>
        </div>
      </>
    )
  }

  submitOrder = async () => {
    const { name, telephone } = this.state
    this.setState({ loading: true })
    if (this.isValid()) {
      setCustomerInfo(name, telephone)
      await submitOrder()
      this.setState({ loading: false })
      navigate("/order/confirmation")
    } else {
      this.setState({ hasError: true })
    }
  }

  getNameError = () => {
    const { name } = this.state
    if (!name) return "Name is required"
  }

  getTelephoneError = () => {
    const { telephone } = this.state
    if (!telephone)
      return "Your phone number is required in case something goes wrong"
  }

  getAstriskColor = (success, error) => {
    if (success) return "green"
    if (error) return "red"
    return "yellow"
  }

  render() {
    const {
      order: { items, totalPrice, deliveryType },
      hasError,
      name,
      telephone,
      loading,
    } = this.state

    return (
      <Layout navTitle={"Taco Love Store"}>
        {loading && (
          <Dimmer active inverted>
            <Loader size="large">Loading</Loader>
          </Dimmer>
        )}
        <CenteredContainer>
          <Segment.Group>
            <FadeIn>
              <Segment>
                <Flex justifyContent="space-between">
                  <div>
                    <h1>Selection</h1>
                    {items.map(item => (
                      <span key={item.id}>
                        {item.name} - {item.quantity}x <br />
                      </span>
                    ))}{" "}
                  </div>
                  <div style={{ textAlign: "right" }}>
                    Total price
                    <br />
                    <h1>€{totalPrice}</h1>
                  </div>
                </Flex>
                <div style={{ textAlign: "right" }}>
                  <Link to="/order/selection-review">Edit</Link>
                </div>
              </Segment>
              <Segment>
                <div>
                  <h1>Order Details</h1>
                  {deliveryType === DELIVERY_TYPES.deliveryAddress &&
                    this.renderDeliveryAddressInfo()}
                  {deliveryType === DELIVERY_TYPES.deliveryLocation &&
                    this.renderDeliveryLocationInfo()}
                  {deliveryType === DELIVERY_TYPES.pickup &&
                    this.renderPickupInfo()}
                </div>
                <div style={{ textAlign: "right" }}>
                  <Link to="/order/delivery/method">Edit</Link>
                </div>
              </Segment>
              <Segment>
                <PaymentSelection />
                <Input
                  onChange={e => this.setState({ name: e.target.value })}
                  fluid
                  error={hasError && this.getNameError()}
                  placeholder="Name"
                  label={{
                    icon: "asterisk",
                    color: this.getAstriskColor(
                      name,
                      hasError && this.getNameError()
                    ),
                  }}
                  size="large"
                  labelPosition="right corner"
                />
                <br />
                <Input
                  onChange={e => this.setState({ telephone: e.target.value })}
                  fluid
                  placeholder="Telephone Number"
                  label={{
                    icon: "asterisk",
                    color: this.getAstriskColor(
                      telephone,
                      hasError && this.getTelephoneError()
                    ),
                  }}
                  size="large"
                  error={hasError && this.getTelephoneError()}
                  labelPosition="right corner"
                />
                <br />
                <div>
                  <Button
                    fluid
                    onClick={this.submitOrder}
                    color="green"
                    size="large"
                  >
                    Place Order
                  </Button>
                </div>
              </Segment>
            </FadeIn>
          </Segment.Group>
        </CenteredContainer>
      </Layout>
    )
  }
}

export default IndexPage
