import React from "react"
import { navigate } from "gatsby"
import Layout from "components/layout"
import SEO from "components/seo"
import "swiper/css/swiper.css"
import {
  Button,
  Label,
  Segment,
  Input,
  Modal,
  Transition,
} from "semantic-ui-react"
import styled from "styled-components"
import { Flex } from "reflexbox"
import FixedBottomButton from "components/FixedBottomButton"
import { getOrder, addItemToOrder, removeItemFromOrder } from "entities/Order"
import { getInfo } from "entities/Store"
import CenteredContainer from "components/CenteredContainer"
import TopLeftButton from "components/TopLeftButton"
import FadeIn from "react-fade-in"
import beforeUnload from "utils/EventListeners/beforeUnload"
const SegmentContainer = styled(Flex)`
  text-align: left;
`

const Description = styled.div`
  font-size: 12px;
  margin-top: 5px;
  opacity: 0.75;
`

class IndexPage extends React.Component {
  state = {
    order: getOrder(),
    itemToRemove: {},
    deleteModalOpen: false,
    visible: false,
  }

  componentDidMount = () => {
    window.removeEventListener("beforeunload", beforeUnload)
    window.scrollTo(0, 0)
    this.setState({ visible: true })
  }

  addQuantity = item => {
    addItemToOrder(item)
    this.setState({ order: getOrder() })
  }

  removeQuantity = item => {
    if (item.quantity === 1) {
      this.openConfirmRemoveModal(item)
    } else {
      this.removeItem(item)
    }
  }

  removeItem = item => {
    removeItemFromOrder(item)
    this.setState({ order: getOrder() })
  }

  openConfirmRemoveModal = item => {
    this.setState({
      itemToRemove: item,
      deleteModalOpen: true,
    })
  }

  renderItems = () => {
    const {
      order: { items },
      visible,
    } = this.state
    let index = 0
    return (
      <>
        <Segment.Group>
          <FadeIn transitionDuration={1000}>
            {items.map(item => {
              index++
              return (
                <Segment key={item.id}>
                  <SegmentContainer>
                    <div style={{ width: "30%" }}>
                      <img width="100%" src={item.image} />
                    </div>
                    <div style={{ paddingLeft: "10px", width: "70%" }}>
                      {" "}
                      {item.name} - €{item.price}
                      <br />
                      <Description>{item.description}</Description>
                      <br />
                      <Button.Group basic size="small">
                        <Button
                          icon="minus"
                          onClick={() => this.removeQuantity(item)}
                        />
                        <Button>{item.quantity}</Button>
                        <Button
                          icon="plus"
                          onClick={() => this.addQuantity(item)}
                        />
                      </Button.Group>
                    </div>
                  </SegmentContainer>
                </Segment>
              )
            })}
          </FadeIn>
        </Segment.Group>
      </>
    )
  }
  render() {
    const {
      order: { items, totalQuantity, totalPrice },
      visible,
    } = this.state
    return (
      <Layout>
        <TopLeftButton onClick={() => navigate("/order")}>Back</TopLeftButton>
        <CenteredContainer padding="0">{this.renderItems()}</CenteredContainer>
        <FixedBottomButton
          onClick={() => navigate("/order/delivery/method")}
          color="yellow"
        >
          Continue (€{totalPrice})
          <Label color="green" floating>
            {totalQuantity}
          </Label>
        </FixedBottomButton>
        <Modal size="tiny" open={this.state.deleteModalOpen}>
          <Modal.Content image>
            <Modal.Description>
              Would you like to remove{" "}
              <b>&quot;{this.state.itemToRemove.name}&quot;</b> from your order?
            </Modal.Description>
          </Modal.Content>
          <Modal.Actions>
            <Button
              negative
              onClick={() => this.setState({ deleteModalOpen: false })}
            >
              No
            </Button>

            <Button
              positive
              icon="checkmark"
              labelPosition="right"
              content="Yes"
              onClick={() => {
                this.removeItem(this.state.itemToRemove)
                this.setState({
                  itemToRemove: false,
                  deleteModalOpen: false,
                })
              }}
            />
          </Modal.Actions>
        </Modal>
      </Layout>
    )
  }
}

export default IndexPage
