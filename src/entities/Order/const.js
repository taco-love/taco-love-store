export const CART_KEY = "cart"

export const DELIVERY_TYPES = {
  pickup: "pickup",
  deliveryLocation: "deliveryLocation",
  deliveryAddress: "deliveryAddress",
}
