import axios from "axios"
import getOrder from "./getOrder"
import saveOrder from "./saveOrder"
import randomstring from "randomstring"
const submitOrder = async () => {
  const order = getOrder()
  order.id = randomstring.generate({
    length: 5,
    charset: "alphanumeric",
    readable: true,
    capitalization: "uppercase",
  })

  saveOrder(order)
  await axios.post(`${process.env.GATSBY_API_BASE_URL}/hello`, order)
}

export default submitOrder
