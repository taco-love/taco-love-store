import getOrder from "./getOrder"
import saveOrder from "./saveOrder"

const removeItemFromOrder = item => {
  let cart = getOrder()
  const { items } = cart
  let existingItem = null
  if (items) {
    existingItem = items.find(cartItem => cartItem.id === item.id)
  } else {
    cart.items = []
  }

  if (existingItem) {
    existingItem.quantity--
  }

  if (existingItem.quantity === 0) {
    const updatedItems = items.filter(item => item.id !== existingItem.id)
    cart.items = updatedItems
  }
  saveOrder(cart)
}

export default removeItemFromOrder
