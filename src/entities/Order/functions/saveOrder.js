import saveSession from "utils/LocalStorage/functions/saveSession"
import { CART_KEY } from "../const"
const saveOrder = cart => {
  cart.totalQuantity = calculateTotalQuantity(cart)
  cart.totalPrice = calculateTotalPrice(cart)
  const cartString = JSON.stringify(cart)
  saveSession(CART_KEY, cartString)
}

const calculateTotalQuantity = cart => {
  const { items } = cart
  let quantity = 0
  items.forEach(item => (quantity += item.quantity))
  return quantity
}

const calculateTotalPrice = cart => {
  const { items } = cart
  let price = 0
  items.forEach(item => (price += item.quantity * item.price))
  return price
}

export default saveOrder
