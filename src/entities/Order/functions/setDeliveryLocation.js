import getOrder from "./getOrder"
import saveOrder from "./saveOrder"

const setDeliveryType = (latitude, longitude) => {
  const order = getOrder()
  order.deliveryLocation = { latitude, longitude }
  saveOrder(order)
}

export default setDeliveryType
