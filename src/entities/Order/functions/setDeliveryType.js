import getOrder from "./getOrder"
import saveOrder from "./saveOrder"

const setDeliveryType = type => {
  const order = getOrder()
  order.deliveryType = type
  saveOrder(order)
}

export default setDeliveryType
