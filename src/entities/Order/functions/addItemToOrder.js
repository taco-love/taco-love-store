import getOrder from "./getOrder"
import saveOrder from "./saveOrder"

const addItemToOrder = item => {
  const cart = getOrder()
  const { items } = cart
  let existingItem = null
  if (items) {
    existingItem = items.find(cartItem => cartItem.id === item.id)
  } else {
    cart.items = []
  }
  if (existingItem) {
    existingItem.quantity++
  } else {
    cart.items.push(item)
  }
  saveOrder(cart)
}

export default addItemToOrder
