import getSession from "utils/LocalStorage/functions/getSession"
import { CART_KEY } from "../const"
const getOrder = () => {
  const cart = getSession(CART_KEY)
  if (cart) {
    return JSON.parse(cart)
  } else {
    return {
      items: [],
      totalQuantity: 0,
      totalPrice: 0,
    }
  }
}

export default getOrder
