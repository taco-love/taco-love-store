import getOrder from "./getOrder"
import saveOrder from "./saveOrder"

const resetOrder = () => {
  const order = getOrder()
  order.items = []
  saveOrder(order)
}

export default resetOrder
