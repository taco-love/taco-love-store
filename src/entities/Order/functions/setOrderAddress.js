import getOrder from "./getOrder"
import saveOrder from "./saveOrder"

const setOrderDeliveryAddress = (address, notes) => {
  const order = getOrder()
  order.deliveryAddress = address
  order.deliveryNotes = notes
  saveOrder(order)
}

export default setOrderDeliveryAddress
