import getOrder from "./getOrder"
import saveOrder from "./saveOrder"

const setCustomerInfo = (name, phone) => {
  const order = getOrder()
  order.customerName = name
  order.customerPhone = phone
  saveOrder(order)
}

export default setCustomerInfo
