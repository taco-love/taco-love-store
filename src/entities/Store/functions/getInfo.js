import getSession from "utils/LocalStorage/functions/getSession"
const STORE_INFO_KEY = "STORE"

const getInfo = ({ save = false } = {}) => {
  let storeInfoString = getSession(STORE_INFO_KEY)
  if (storeInfoString) {
    return JSON.parse(storeInfoString)
  } else return {}
}

export default getInfo
