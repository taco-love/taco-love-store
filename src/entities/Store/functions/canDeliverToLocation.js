import getDistance from "geolib/es/getDistance"
import getInfo from "./getInfo"
import getDeliveryRadius from "./getDeliveryRadius"
const canDeliverToLocation = ({ latitude, longitude }) => {
  const storeCoordinates = getInfo().deliveryCenter
  if (storeCoordinates) {
    const distance = getDistance(storeCoordinates, { latitude, longitude })

    return distance <= getDeliveryRadius()
  }
}

export default canDeliverToLocation
