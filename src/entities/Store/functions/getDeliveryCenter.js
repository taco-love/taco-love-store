import getInfo from "./getInfo"

const getDeliveryCenter = () => {
  return getInfo().deliveryCenter || { longitude: 0, latitude: 0 }
}

export default getDeliveryCenter
