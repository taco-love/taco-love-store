import getInfo from "./getInfo"

const getDeliveryRadius = () => {
  return getInfo().deliveryRadius
}

export default getDeliveryRadius
