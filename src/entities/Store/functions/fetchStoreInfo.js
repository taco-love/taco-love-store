import Client from "utils/Prismic/Client"
import Prismic from "prismic-javascript"
import saveSession from "utils/LocalStorage/functions/saveSession"
const STORE_INFO_KEY = "STORE"

const fetchStoreInfo = async () => {
  const StoreResponse = await Client.query(
    Prismic.Predicates.at("document.type", "store")
  )
  const storeData = StoreResponse.results[0].data
  const storeInfo = {
    name: storeData.name[0].text,
    deliveryCenter: storeData.delivery_center,
    deliveryRadius: storeData.delivery_radius,
    location: storeData.location,
    address: storeData.address,
    open: storeData.open,
  }
  const storeInfoString = JSON.stringify(storeInfo)
  saveSession(STORE_INFO_KEY, storeInfoString)
}

export default fetchStoreInfo
