import getInfo from "./getInfo"
const getCoordinates = () => {
  const coordinates = getInfo().location
  if (coordinates) return coordinates
  else return {}
}

export default getCoordinates
