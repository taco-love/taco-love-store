import Client from "utils/Prismic/Client"
import Prismic from "prismic-javascript"

const getMenu = async () => {
  const MenuCategoryPromise = Client.query(
    Prismic.Predicates.at("document.type", "menu_category"),
    {
      fetchLinks: [
        "menu_item.name",
        "menu_item.description",
        "menu_item.price",
        "menu_item.image",
      ],
    }
  )
  const MenuItemsPromise = Client.query(
    Prismic.Predicates.at("document.type", "menu_item")
  )

  const [MenuCategoryResponse, MenuItemsResponse] = await Promise.all([
    MenuCategoryPromise,
    MenuItemsPromise,
  ])

  const allItems = MenuItemsResponse.results.map(item => {
    const data = item.data
    return {
      id: data.name[0].text,
      name: data.name[0].text,
      description: data.description[0].text,
      image: data.image.url,
      price: data.price,
      quantity: 0,
    }
  })

  const menu = []
  menu.push({
    title: "All Menu Items",
    items: allItems,
  })

  MenuCategoryResponse.results.map(category => {
    const items = []
    const categoryData = category.data
    const { menu_items } = categoryData || {}
    menu_items.forEach(itemData => {
      const menuItem = itemData.menu_item.data
      if (menuItem) {
        const { name, price, description, image } = menuItem
        items.push({
          id: name[0].text,
          name: name[0].text,
          description: description[0].text,
          price: price,
          quantity: 0,
          image: image.url,
        })
      }
    })
    menu.push({
      title: categoryData.category_name[0].text,
      items: items,
    })
  })

  return menu
}

export default getMenu
