import Client from "utils/Prismic/Client"
import Prismic from "prismic-javascript"

const getNotification = async () => {
  const Notification = await Client.query(
    Prismic.Predicates.at("document.type", "store_notification")
  )

  const results = Notification.results
  if (!results) return null

  const data = results[0].data
  return {
    title: data.title[0].text,
    content: data.content[0].text,
    image: data.image.url,
  }
}

export default getNotification
