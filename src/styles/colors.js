export const teal = "#00b5ad"
export const primary = {
  o100: "#fdc009",
}
export const green = "#A8D056"

export const white = {
  o30: "rgba(255,255,255,0.3)",
}

export const black = {
  o10: "rgba(0,0,0,0.1)",
}
