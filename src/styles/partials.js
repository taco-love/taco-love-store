import { css } from "styled-components"
import { black } from "styles/colors"

const lhCrop = lineHeight => {
  return `
    &::before {
      content: '';
      display: block;
      height: 0;
      width: 0;
      margin-top: calc((1 - ${lineHeight}) * 0.5em);
    }
  `
}

const clickable = css`
  cursor: pointer;
`

const cardShadow = css`
  box-shadow: 0 0 20px 2px ${black.o10};
`

export { lhCrop, clickable, cardShadow }
